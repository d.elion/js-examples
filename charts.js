function history(){
  var month =  urlParams.get('month') || latest[0].monthName.trim();
  var labels = totalHistory.map(function(e){
    return e.year;
  });
  var board = totalHistory.map(function(e) {
    var format = Number(e.boardings);
    return format;
  });

  var ctx = document.getElementById('history').getContext('2d');
  var hConfig = {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
          label: 'Historical '+month+' Total Ridership',
          data: board,
          backgroundColor: '#0065bd',
          borderColor: '#0065bd',
      }]
    },
    options: {
      responsive: false,
      maintainAspectRatio: false,
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            var tooltipValue = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
            return parseInt(tooltipValue).toLocaleString();
          }
        }
      },
      legend: {
        labels: {
          boxWidth: 0,
        }
     },
      scales:{
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value, index, values) {
              return value.toLocaleString();
            }
          }
        }]
      }
    },
  };

  new Chart(ctx, hConfig);
}

