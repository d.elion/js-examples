var data = Drupal.settings[0].table; //drupal data 
var latest = Drupal.settings[0].latest;

function remove_duplicates(arr) {
  var obj = {};
  var ret_arr = [];
  for (var i = 0; i < arr.length; i++) {
    arr = arr.filter(Boolean);
      obj[arr[i]] = true;
  }
  for (var key in obj) {
      ret_arr.push(key);
  }
  return ret_arr;
}

function railTable() {
  var railTrends = data.railTrends;
  var dayTotals = data.dayTotals;
  var rail = [];
  var branch = railTrends.reduce(function (r, a) {
    r[a.placeAbbrev] = r[a.placeAbbrev] || [];
    var format = Number(a.averageRides);
    rail.push(a.serviceGroup);
    r[a.placeAbbrev].push(format.toLocaleString());
    return r;
  }, Object.create(null));

  var serviceName = data.railTrends[0].serviceName;
  var branchDiv = document.getElementsByClassName('rider')[0];
  var yearDiv = document.getElementsByClassName('rider-year')[0];
  branchDiv.innerHTML = serviceName;
  yearDiv.innerHTML = data.railTrends[0].year;

  var branches = [];
  rail = remove_duplicates(rail);

  Object.entries(branch).forEach(entry => {
    entry[1].unshift(rail[0]);
    entry[1].unshift(entry[0]);
    branches.push(entry[1]);
  });

  var columns =[],
  columnsTitle =[];
  for( var j = 0; railTrends.length > j; j++){
    titles = railTrends[j].dayType;
    columns.push(titles);
  }
  columns = remove_duplicates(columns); //remove duplicate

  //totals
  var totals = [];
  for(var l = 0; dayTotals.length > l; l++){
    format = Number(dayTotals[l].totalRides);
    totals.push(format.toLocaleString());
  }
  totals.unshift(' ');
  totals.unshift('Totals');
  branches.push(totals);

  for( var e=0; columns.length > e; e++){
    columnsTitle.push({title : columns[e]});
  }
  columnsTitle.unshift({title: 'Lines'});
  columnsTitle.unshift({title: 'Station'}); //adding column title, working
  jQuery.fn.dataTable.ext.errMode = 'none';
  jQuery('#table').dataTable({
    data: branches,
    columns:columnsTitle,
    paging: false,
    scrollX: true,
    order:[],
  });

}

document.addEventListener('DOMContentLoaded', function() {
  railTable();
}, false);

